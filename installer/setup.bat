@echo off

setlocal enabledelayedexpansion

set dir=%~dp0
set "dir=!dir:\=\\!"

@echo [erlang] > .\erl9.0\bin\erl.ini
@echo Bindir=%dir%erl9.0\\erts-9.0\\bin >> .\erl9.0\bin\erl.ini
@echo Progname=erl >> .\erl9.0\bin\erl.ini
@echo Rootdir=%dir%erl9.0 >> .\erl9.0\bin\erl.ini

endlocal
