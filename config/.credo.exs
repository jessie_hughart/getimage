%{
  configs: [
    %{
      name: "default",
      files: %{
        included: ["lib/", "src/", "web/", "apps/"],
        excluded: []
      },
      checks: [
		{Credo.Check.Refactor.CyclomaticComplexity, max_complexity: 12},
		{Credo.Check.Refactor.PipeChainStart, false},
        {Credo.Check.Readability.SpaceAfterCommas, false}
      ]
    }
  ]
}
