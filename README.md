# GetImage
### GetImage is a Website Image scrapper written in Elixir

### Get latest build here:
https://bitbucket.org/jessie_hughart/getimage/downloads/getimage

### Requirements
* runtime requirements Erlang 20.0
https://www.erlang-solutions.com/resources/download.html

* build requirements Elixir 1.5.1
https://elixir-lang.org/install.html

### Build Instructions
* mix deps.get
* mix escript.build

### Usage:
**escript getimage** URL [Options] [FileExts]

### Example:
**escript getimage** _"http://www.google.com"_ --path=images

```
Options
  -p --path=<path>      Path to store images
  -c --content          Only show images from content="..."
  -i --img              Pull down urls prefixed with either img tag or img/image meta data tag
  -m --meta             filter to meta data tags
  -s --src              Only show images from src="..."
     --downloads=<num>  Sets the number of parallel downloads, defaults to 8
     --nossl            Ignore invalid certificate errors
     --showsize         Shows the size of the file
     --showskipped      Shows files skipped by the size flag
     --size             Ignore files smaller than this (in bytes)

File Extenions
  -a --all              Include all image types
     --jpg              Include jpg images
     --png              Include png images
     --bmp              Include bmp images
     --gif              Include gif images
	 --OtherExt=<ext>   Download file of this extension
```

### Notes
* by default searchs for jpg, bmp, and png files
* -i and -m are both useful for instagram pics (only one is needed)
* for -m, -i, -c -s, they are processed in this order (so you can combine meta and image for instance), they can however be supplied in any order
* OtherExt does nto check if it's a valid image extension and will download any file with the given extension.  For example --OtherExt=pdf will download pdf files
