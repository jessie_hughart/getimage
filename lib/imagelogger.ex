defmodule Getimage.Logger do
  @moduledoc """
  this module provides color logging to stand out
  """

  def log({type, msg}) do
    {{year,month,day},{hour,minute,second}} = :calendar.local_time()
    [
      :steelblue,
      Integer.to_string(year),"/",
      padnum(month, 2, "0"),"/",
      padnum(day, 2, "0"), " ",
      padnum(hour, 2, "0"), ":",
      padnum(minute, 2, "0"),"/",
      padnum(second, 2, "0"),
      :default_color, " [",
      case type do
        :error -> [:red, :bright, "Error"]
        :warn -> [:yellow, :bright, "Warn"]
        :info -> [:green, :bright, "Info"]
      end,
      :default_color, "] ",
      msg
    ]
    |> Bunt.puts
  end

  defp padnum(number, pad, padstr) do
    number
    |> Integer.to_string
    |> String.pad_leading(pad, padstr)
  end
end
