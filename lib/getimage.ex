defmodule Getimage do
  @moduledoc """
  this module provides an escript web scrapper, see usage for more info
  """

  import Getimage.Logger

  def main([]) do
    IO.puts ""
    IO.puts "Usage: getimage URL [Options] [FileExts]"
    IO.puts ""
    IO.puts "  Website Image scrapper"
    IO.puts ""
    IO.puts "Example:"
    IO.puts ""
    IO.puts "  escript getimage \"http://www.google.com\" --path=images"
    IO.puts ""
    IO.puts "Options"
    IO.puts "  -p --path=<path>      Path to store images"
    IO.puts "  -c --content          Only show images from content=\"...\""
    IO.puts "  -i --img              Pull down urls prefixed with either img tag or img/image meta data tag"
    IO.puts "  -m --meta             filter to meta data tags"
    IO.puts "  -s --src              Only show images from src=\"...\""
    IO.puts "     --downloads=<num>  Sets the number of parallel downloads, defaults to 8"
    IO.puts "     --nossl            Ignore invalid certificate errors"
    IO.puts "     --showsize         Shows the size of the file"
    IO.puts "     --showskipped      Shows files skipped by the size flag"
    IO.puts "     --size             Ignore files smaller than this (in bytes)"
    IO.puts ""
    IO.puts "File Extenions"
    IO.puts "  -a --all              Include all image types"
    IO.puts "     --jpg              Include jpg images"
    IO.puts "     --png              Include png images"
    IO.puts "     --bmp              Include bmp images"
    IO.puts "     --gif              Include gif images"
    IO.puts "     --OtherExt=<ext>   Download file of this extension"
    IO.puts ""
    IO.puts "Notes"
    IO.puts "  by default searchs for jpg, bmp, and png files"
    IO.puts ""
    IO.puts "  -i and -m are both useful for instagram pics (only one is needed)"
    IO.puts ""
    IO.puts "  for -m, -i, -c -s, they are processed in this order (so you can combine meta and image for instance),"
    IO.puts "  they can however be supplied in any order"
    IO.puts ""
    IO.puts "  OtherExt does nto check if it's a valid image extension and will download any file"
    IO.puts "  with the given extension.  For example --OtherExt=pdf will download pdf files"
    IO.puts ""
  end

  def main([url|args]) do
    {opts, _, _} = OptionParser.parse(args,
      switches: [
        # where to find image urls
        content: :boolean,
        img: :boolean,
        meta: :boolean,
        src: :boolean,

        # misc options
        downloads: :integer,
        nossl: :boolean,
        path: :string,
        showsize: :boolean,
        showskipped: :boolean,
        size: :integer,

        # image file ext options
        jpg: :boolean,
        bmp: :boolean,
        png: :boolean,
        gif: :boolean,
        all: :boolean,
        OtherExt: :string
      ],
      aliases: [
        a: :all,
        c: :content,
        i: :img,
        m: :meta,
        p: :path,
        s: :src
      ])

    path = Keyword.get(opts, :path, "./")
    log({:info, "URL: #{url}"})
    log({:info, "PATH #{path}"})

    case getresponse(url, opts[:nossl]) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} -> getImages(body, path, opts)
      {:ok, %HTTPoison.Response{status_code: code}} -> log({:error, "Error Code: #{code}"})
      {:error, %HTTPoison.Error{reason: reason}} -> log({:error, "#{inspect reason}"})
    end
  end

  defp filterbody(true, :meta, body), do:   Regex.scan(~r/meta.*/, body) |> Enum.join(" ")
  defp filterbody(true, :img, body), do: Regex.scan(~r/(?:img|image).*/, body) |> Enum.join(" ")
  defp filterbody(true, :content, body), do: Regex.scan(~r/content.*/, body) |> Enum.join(" ")
  defp filterbody(true, :src, body), do: Regex.scan(~r/src.*/, body)  |> Enum.join(" ")
  defp filterbody(_, _, body), do: body

  def getImages(body, path, opts) do
    body = filterbody(opts[:meta], :meta, body)
    body = filterbody(opts[:img], :img, body)
    body = filterbody(opts[:content], :content, body)
    body = filterbody(opts[:src], :src, body)

    regexstr = "http[^\"']+(?:" <> getfileexts(opts) <> ")"

    {:ok, rx} = Regex.compile(regexstr)

    Regex.scan(rx, body)
    |> List.flatten
    |> Stream.uniq
    |> Task.async_stream(Getimage, :downloadimage, [path, opts], [max_concurrency: Keyword.get(opts, :downloads, 8), timeout: :infinity])
    |> Enum.to_list()
  end


  def downloadimage(url, path, opts) do
    url = String.replace(url, "\\", "")
    fileregex = "[^\/]+.(?:" <> getfileexts(opts) <> ")"
    {:ok, filerx} = Regex.compile(fileregex)
    [filename] = Regex.run(filerx, url)

    imageinfo = [
      :color176, "getting image: ",
      :default_color,
      String.pad_trailing(filename, 100, " ")
    ]

    case getresponse(url, opts[:nossl]) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        imagesize = byte_size(body)
        if imagesize >= Keyword.get(opts, :size, 0) do
          case File.write(Path.join(path, filename), body) do
            :ok -> log({:info, imageinfo ++ [
                         :green,
                         :bright,
                         if Keyword.get(opts, :showsize) do String.pad_leading("(size: #{imagesize})", 25, " ") else "" end,
                         String.pad_leading("Finished", 10, " ")]})
            _ -> log({:error, imageinfo ++ [
                       :red,
                       :bright,
                       String.pad_leading("File Error", 10, " ")]})
          end
        else
          if Keyword.has_key?(opts, :showskipped) do
            log({:warn, imageinfo ++ [
                  :yellow,
                  :bright,
                  if Keyword.get(opts, :showsize) do String.pad_leading("(size: #{imagesize})", 25, " ") else "" end,
                  String.pad_leading("skipping", 10, " ")]})
          end
        end
      {:ok, %HTTPoison.Response{status_code: code}} -> log({:error, imageinfo ++ [:red, :bright, String.pad_leading("Error #{code}", 10, " ")]})
      {:error, %HTTPoison.Error{reason: reason}} -> log({:error, "#{inspect reason}"})
    end
  end

  defp getresponse(url, true), do: HTTPoison.get(url, [], hackney: [:insecure])
  defp getresponse(url, _), do: HTTPoison.get(url, [])

  defp getfileexts(opts) do
    opt = [
    if opts[:jpg] || opts[:all] do "jpg" else nil end,
    if opts[:bmp] || opts[:all] do "bmp" else nil end,
    if opts[:png] || opts[:all] do "png" else nil end,
    if opts[:gif] || opts[:all] do "gif" else nil end,
    opts[:OtherExt]
    ]
    |> Stream.filter(fn x -> if x, do: x end) # filter out nil values
    |> Enum.join("|") # use pipe operator for use in regex

    if opt == "" do "jpg|bmp|png" else opt end # default image types
  end
end
