defmodule Getimage.Mixfile do
  use Mix.Project

  def project do
    [
      app: :getimage,
      version: "0.1.0",
      elixir: "~> 1.5",
      start_permanent: Mix.env == :prod,
      deps: deps(),
	  escript: escript()
    ]
  end

  def application do
    [
      extra_applications: [],
    ]
  end

  defp deps do
    [
	  {:httpoison, "~> 0.13"},
	  {:bunt, "~> 0.1.0"}
    ]
  end

  def escript do
    [main_module: Getimage]
  end
end
